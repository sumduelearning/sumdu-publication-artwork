### Installation

Requires [Node.js](https://nodejs.org/) v4+ to run.
```sh
$ npm i
```
## How to run

```sh
$ npm run start
```
Run webserver for develop
Open yuor favorite browser http://localhost:8080/
```sh
$ npm run build
```
Build for production
